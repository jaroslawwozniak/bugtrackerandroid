package com.whallalabs.bugtracker.history.customdata;

import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-23.
 */
public interface ICustomDataAddition {
    List<ICustomData> getCustomData();

    void setCustomData(List<ICustomData> customData);
}
