package com.whallalabs.bugtracker.history.customdata;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-23.
 */
public interface ICustomData {

    String getKey();

    void setKey(String key);

    String getValue();

    void setValue(String value);

}
