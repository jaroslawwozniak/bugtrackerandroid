package com.whallalabs.bugtracker.history.holder;

import android.util.Log;

import com.snappydb.SnappydbException;
import com.whallalabs.bugtracker.BugTracker;
import com.whallalabs.bugtracker.history.entry.DefaultHistoryEntry;
import com.whallalabs.bugtracker.history.entry.IHistoryEntry;
import com.whallalabs.bugtracker.history.entry.error.IError;
import com.whallalabs.bugtracker.history.entry.session.ISession;
import com.whallalabs.bugtracker.history.entry.trackevent.ITrackEvent;
import com.whallalabs.bugtracker.model.ModelFactory;
import com.whallalabs.bugtracker.model.TrackEvent;
import com.whallalabs.bugtracker.util.database.DbHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-13.
 */
public class SessionHistorySnappyDBHolder implements ISessionHistoryHolder {

    private static final String KEY = "SessionHistorySnappyDBHolder_";
    private List<IHistoryEntry> _entries = new ArrayList<>();
    private ModelFactory _modelFactory;
    private List<ITrackEvent> _trackEventList = new ArrayList<>();
    private int _cachedCount = 0;

    public SessionHistorySnappyDBHolder(ModelFactory modelFactory) {
        _modelFactory = modelFactory;
        load();
    }

    @Override
    public void addEntry(IHistoryEntry historyEntry) {
        _entries.add(historyEntry);
        try {
            DbHelper.getSynchronizedDB().put(KEY + historyEntry.getId(), historyEntry);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addError(IError error) {
        error.setTrackEvents(_trackEventList);
        putErrorToHistory(error);
    }

    @Override
    public void addTrackEvent(String eventName) {
        TrackEvent trackEvent = _modelFactory.getTrackEvent(eventName);
        _trackEventList.add(trackEvent);
    }

    @Override
    public void onEntryUpdated(IHistoryEntry historyEntry) {
        try {
            DbHelper.getSynchronizedDB().put(KEY + historyEntry.getId(), historyEntry);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteEntry(IHistoryEntry historyEntry) {
        try {
            DbHelper.getSynchronizedDB().del(KEY + historyEntry.getId());
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        historyEntry.setHasDataToSend(false);
    }

    @Override
    public IHistoryEntry getCurrentHistoryEntry() {
        return _entries.size() > 0 ? _entries.get(_entries.size() - 1) : null;
    }


    @Override
    public List<IHistoryEntry> getEntries() {
        return _entries;
    }

    @Override
    public int getEntriesLoadedFromStorageCount() {
        return _cachedCount;
    }

    private void load() {
        Log.d(BugTracker.LOGCAT_TAG, "HistoryHolderInit");

        try {
            String[] keys = DbHelper.getSynchronizedDB().getKeyArray(KEY);
            List<String> keysList = Arrays.asList(keys);
            _cachedCount = keysList.size();

            for (String string : keysList) {
                _entries.add(DbHelper.getSynchronizedDB().getObject(string, DefaultHistoryEntry.class));
            }
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
        Log.d(BugTracker.LOGCAT_TAG, "HistoryHolderOpened - Entries Found:" + _cachedCount);
    }

    private void putErrorToHistory(IError error) {
        IHistoryEntry currentEntry = getCurrentHistoryEntry();
        ISession currentSession = currentEntry.getSession();
        currentSession.getErrorList().add(error);
        currentEntry.setHasDataToSend(true);
        onEntryUpdated(currentEntry);
    }


}
