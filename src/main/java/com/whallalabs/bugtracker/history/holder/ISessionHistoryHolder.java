package com.whallalabs.bugtracker.history.holder;

import com.whallalabs.bugtracker.history.entry.IHistoryEntry;
import com.whallalabs.bugtracker.history.entry.error.IError;

import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public interface ISessionHistoryHolder {

    void addEntry(IHistoryEntry historyEntry);

    void addError(IError error);

    void addTrackEvent(String event);

    void onEntryUpdated(IHistoryEntry historyEntry);

    void deleteEntry(IHistoryEntry historyEntry);

    IHistoryEntry getCurrentHistoryEntry();

    List<IHistoryEntry> getEntries();

    int getEntriesLoadedFromStorageCount();

}
