package com.whallalabs.bugtracker.history.entry.error;

import com.whallalabs.bugtracker.history.customdata.ICustomDataAddition;
import com.whallalabs.bugtracker.history.entry.trackevent.ITrackEvent;

import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-15.
 */
public interface IError extends ICustomDataAddition {
    boolean isHandled();

    void setHandled(boolean isHandled);

    List<ITrackEvent> getTrackEvents();

    void setTrackEvents(List<ITrackEvent> trackEvents);
}
