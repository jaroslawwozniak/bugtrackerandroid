package com.whallalabs.bugtracker.history.entry.trackevent;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-23.
 */
public interface ITrackEvent {

    Long getTime();

    void setTime(Long time);

    String getEvent();

    void setEvent(String event);

}
