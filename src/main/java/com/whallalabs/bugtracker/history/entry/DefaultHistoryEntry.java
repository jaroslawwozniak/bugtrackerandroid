package com.whallalabs.bugtracker.history.entry;

import com.whallalabs.bugtracker.history.entry.session.ISession;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public class DefaultHistoryEntry extends HistoryEntry {

    private boolean _isInitiatedOnRemote = false;
    private boolean _hasDataToSend = false;

    public DefaultHistoryEntry() {

    }

    public DefaultHistoryEntry(ISession session) {
        super(session);
    }

    @Override
    public boolean isInitiatedOnRemote() {
        return _isInitiatedOnRemote;
    }

    @Override
    public void setInitiatedOnRemote(boolean isInitiated) {
        _isInitiatedOnRemote = isInitiated;
    }

    @Override
    public boolean hasDataToSend() {
        return _hasDataToSend;
    }

    @Override
    public void setHasDataToSend(boolean hasDataToSend) {
        _hasDataToSend = hasDataToSend;
    }

}
