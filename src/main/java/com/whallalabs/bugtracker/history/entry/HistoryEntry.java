package com.whallalabs.bugtracker.history.entry;

import com.whallalabs.bugtracker.history.entry.session.ISession;

import java.util.UUID;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public abstract class HistoryEntry implements IHistoryEntry {
    ISession _session;
    String _id;

    public HistoryEntry() {

    }

    public HistoryEntry(ISession session) {
        _session = session;
        _id = UUID.randomUUID().toString();
    }

    @Override
    public String getId() {
        return _id;
    }

    @Override
    public ISession getSession() {
        return _session;
    }

}
