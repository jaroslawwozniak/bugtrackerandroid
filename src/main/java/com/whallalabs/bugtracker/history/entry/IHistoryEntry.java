package com.whallalabs.bugtracker.history.entry;

import com.whallalabs.bugtracker.history.entry.session.ISession;

import java.io.Serializable;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public interface IHistoryEntry extends Serializable {
    boolean isInitiatedOnRemote();

    void setInitiatedOnRemote(boolean isInitiated);

    boolean hasDataToSend();

    void setHasDataToSend(boolean hasDataToSend);

    String getId();

    ISession getSession();
}
