package com.whallalabs.bugtracker.history.entry.session;

import com.whallalabs.bugtracker.history.entry.error.IError;

import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public interface ISession {
    List<IError> getErrorList();

    void addCustomData(String key, String value);
}
