package com.whallalabs.bugtracker.sender.apisender;

import com.whallalabs.bugtracker.history.entry.IHistoryEntry;
import com.whallalabs.bugtracker.sender.ISender;
import com.whallalabs.bugtracker.sender.ISenderCallbacks;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-13.
 */
public class BugTrackerAPISender implements ISender {

    public BugTrackerAPISender() {

    }

    @Override
    public void postEntry(IHistoryEntry historyEntry, ISenderCallbacks senderCallbacks) {
        new PostSessionAsyncTask(historyEntry, senderCallbacks).execute();
    }

}
