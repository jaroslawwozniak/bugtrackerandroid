package com.whallalabs.bugtracker.sender.apisender;

import android.os.AsyncTask;
import android.util.Log;

import com.whallalabs.bugtracker.BugTracker;
import com.whallalabs.bugtracker.history.entry.IHistoryEntry;
import com.whallalabs.bugtracker.history.entry.session.ISession;
import com.whallalabs.bugtracker.sender.ISenderCallbacks;
import com.whallalabs.bugtracker.sender.restadapter.RestAdapterHolder;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-17.
 */
public class PostSessionAsyncTask extends AsyncTask<Void,Void,Void> {

    IHistoryEntry _historyEntry;
    ISenderCallbacks _senderCallbacks;


    interface PostSessionAPI {
        @POST("/errors")
        void postSession(
               @Body
               ISession session,
               Callback<Object> cb
        );
    }

    public PostSessionAsyncTask(IHistoryEntry historyEntry, ISenderCallbacks senderCallbacks) {
        _historyEntry = historyEntry;
        _senderCallbacks = senderCallbacks;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.d(BugTracker.LOGCAT_TAG, "Post START ID: " + _historyEntry.getId() + " Errors #" + _historyEntry.getSession().getErrorList().size());
        try {
            PostSessionAPI postApi = RestAdapterHolder.getStaticInstance().getRestAdapter().create(PostSessionAPI.class);
            Callback cb = new Callback<Object>() {
                @Override
                public void success(Object o, Response response) {
                    _senderCallbacks.onSuccess(_historyEntry);
                    Log.d(BugTracker.LOGCAT_TAG, "Post OK ID: " + _historyEntry.getId() + " Errors #" + _historyEntry.getSession().getErrorList().size());
                }

                @Override
                public void failure(RetrofitError error) {
                    _senderCallbacks.onFailure(_historyEntry);
                    Log.d(BugTracker.LOGCAT_TAG, "Post ERROR ID: " + _historyEntry.getId() + " Errors #" + _historyEntry.getSession().getErrorList().size());
                }
            };
            postApi.postSession(_historyEntry.getSession(), cb);
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }
}
