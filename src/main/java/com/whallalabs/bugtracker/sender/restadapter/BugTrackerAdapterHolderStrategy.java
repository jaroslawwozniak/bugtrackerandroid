package com.whallalabs.bugtracker.sender.restadapter;

import android.util.Pair;

import com.whallalabs.bugtracker.BuildConfig;

import java.util.List;

import retrofit.RequestInterceptor;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-17.
 */
public class BugTrackerAdapterHolderStrategy implements IRestAdapterStrategy {

    private String _apiUrl;
    private List<Pair<String, String>> _headerList;

    public BugTrackerAdapterHolderStrategy(String apiUrl, List<Pair<String, String>> pairList) {
        _apiUrl = apiUrl;
        _headerList = pairList;
    }

    @Override
    public void interceptRequest(RequestInterceptor.RequestFacade request) {
        for (Pair<String, String> header : _headerList) {
            request.addHeader(header.first, header.second);
        }
    }

    @Override
    public String getAPIUrl() {
        return _apiUrl;
    }

    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }
}
