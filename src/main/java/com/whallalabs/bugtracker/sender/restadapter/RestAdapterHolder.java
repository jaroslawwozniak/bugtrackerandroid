package com.whallalabs.bugtracker.sender.restadapter;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.android.MainThreadExecutor;
import retrofit.client.OkClient;

/**
 * Created by Jaroslaw Wozniak  on 2015-06-17.
 */
public class RestAdapterHolder {
    private static RestAdapterHolder _instance;
    private IRestAdapterStrategy _interceptRequestStrategy;
    private RestAdapter _restAdapter;
    private ExecutorService _executorService;

    private RequestInterceptor _requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            if (_interceptRequestStrategy != null) {
                _interceptRequestStrategy.interceptRequest(request);
            }
        }
    };

    public static RestAdapterHolder getStaticInstance() {
        if (_instance == null)
            _instance = new RestAdapterHolder();
        return _instance;
    }

    public static RestAdapterHolder getNewInstance() {
        RestAdapterHolder result = new RestAdapterHolder();
        return result;
    }

    public synchronized void init(IRestAdapterStrategy interceptRequestStrategy) {
        _interceptRequestStrategy = interceptRequestStrategy;
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(_interceptRequestStrategy.getAPIUrl());
        if (_interceptRequestStrategy.isDebug()) {
            builder.setLogLevel(RestAdapter.LogLevel.FULL);
        }
        _executorService = Executors.newCachedThreadPool();
        builder.setExecutors(_executorService, new MainThreadExecutor());
        // builder.setClient(new MockingOkClient());
        builder.setClient(new OkClient());
        builder.setRequestInterceptor(_requestInterceptor);
        _restAdapter = builder.build();
    }

    /* http://stackoverflow.com/questions/18131382/using-squares-retrofit-client-is-it-possible-to-cancel-an-in-progress-request */
    public void cancelRequests() {
        List<Runnable> pendingAndOngoing = _executorService.shutdownNow();
        // probably await for termination.
    }

    public RestAdapter getRestAdapter() {
        return _restAdapter;
    }
}
