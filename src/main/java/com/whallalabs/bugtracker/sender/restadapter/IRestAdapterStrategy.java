package com.whallalabs.bugtracker.sender.restadapter;

import retrofit.RequestInterceptor;

/**
 * Created by Jaroslaw Wozniak  on 2015-06-17.
 */
public interface IRestAdapterStrategy {
    void interceptRequest(RequestInterceptor.RequestFacade request);

    String getAPIUrl();

    boolean isDebug();
}
