package com.whallalabs.bugtracker.sender.mockedsender;

import android.util.Log;

import com.whallalabs.bugtracker.BugTracker;
import com.whallalabs.bugtracker.history.entry.IHistoryEntry;
import com.whallalabs.bugtracker.sender.ISender;
import com.whallalabs.bugtracker.sender.ISenderCallbacks;

import java.util.Random;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-28.
 */
public class MockedSender implements ISender {

    @Override
    public void postEntry(IHistoryEntry historyEntry, ISenderCallbacks senderCallbacks) {
        Log.d(BugTracker.LOGCAT_TAG, "Post START ID: " + historyEntry.getId() + " Errors #" + historyEntry.getSession().getErrorList().size());

        if (senderCallbacks != null) {
            if (getRandomBoolean()) {
                senderCallbacks.onSuccess(historyEntry);
                Log.d(BugTracker.LOGCAT_TAG, "Post OK ID: " + historyEntry.getId() + " Errors #" + historyEntry.getSession().getErrorList().size());
            } else {
                senderCallbacks.onFailure(historyEntry);
                Log.d(BugTracker.LOGCAT_TAG, "Post ERROR ID: " + historyEntry.getId() + " Errors #" + historyEntry.getSession().getErrorList().size());
            }
        }
    }

    public boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }
}
