package com.whallalabs.bugtracker.sender;

import com.whallalabs.bugtracker.history.entry.IHistoryEntry;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-21.
 */
public interface ISenderCallbacks {
    void onSuccess(IHistoryEntry historyEntry);

    void onFailure(IHistoryEntry historyEntry);
}
