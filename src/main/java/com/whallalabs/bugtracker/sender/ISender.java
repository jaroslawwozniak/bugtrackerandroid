package com.whallalabs.bugtracker.sender;

import com.whallalabs.bugtracker.history.entry.IHistoryEntry;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-13.
 */
public interface ISender {
    void postEntry(IHistoryEntry historyEntry, ISenderCallbacks senderCallbacks);
}
