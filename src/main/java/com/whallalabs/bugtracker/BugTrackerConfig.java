package com.whallalabs.bugtracker;

import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-29.
 */
public class BugTrackerConfig {

    private final static String API_URL = "http://bugtrackererrors.azurewebsites.net:80/api/";
    private List<Pair<String, String>> _headers = new ArrayList<>();
    private String API_KEY = "app-key";

    public String getApiUrl() {
        return API_URL;
    }

    public void addHeader(String key, String value) {
        _headers.add(new Pair<>(key, value));
    }

    public List<Pair<String, String>> getHeaders() {
        return _headers;
    }

    public void addAPIKey(String key) {
        _headers.add(new Pair<>(API_KEY, key));
    }
}
