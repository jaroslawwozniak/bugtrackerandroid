package com.whallalabs.bugtracker.historymanager;

import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.whallalabs.bugtracker.HandledErrorBuilder;
import com.whallalabs.bugtracker.errorhandler.IHandlerEventsListener;
import com.whallalabs.bugtracker.history.entry.error.IError;
import com.whallalabs.bugtracker.historymanager.strategy.HistoryManagerStrategy;
import com.whallalabs.bugtracker.model.ModelFactory;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-16.
 */
public class ErrorHistoryManager implements IHistoryManager, IHandlerEventsListener {

    private HistoryManagerStrategy _managerStrategy;
    private ModelFactory _modelFactory;
    private Application _application;

    public ErrorHistoryManager(HistoryManagerStrategy managerStrategy,
                               ModelFactory sessionModelFactory,
                               Application application) {
        _managerStrategy = managerStrategy;
        _modelFactory = sessionModelFactory;
        _application = application;
    }

    @Override
    public void onInit() {
        setupActivityCallbacks();
        _managerStrategy.onAppStart();
    }

    @Override
    public void addSessionCustomData(String key, String value) {
        _managerStrategy.onAddSessionCustomData(key, value);
    }

    @Override
    public void onUnhandledError(Thread thread, Throwable throwable) {
        IError error = _modelFactory.getError(ModelFactory.ErrorType.DEFAULT_UNHANDLED, throwable);
        _managerStrategy.onErrorHandled(error);
        Log.d("BugTracker", "onUnhandledError()");
    }

    @Override
    public void onHandledError(HandledErrorBuilder handledErrorBuilder) {
        IError error = _modelFactory.getError(ModelFactory.ErrorType.DEFAULT_HANDLED, handledErrorBuilder.getThrowable(), handledErrorBuilder.getCustomData());
        _managerStrategy.onErrorHandled(error);
        Log.d("BugTracker", "onHandledError()");
    }

    private void setupActivityCallbacks() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            _application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
                @Override
                public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                    _managerStrategy.onActivityLifecycle("onActivityCreated: ", activity);
                }

                @Override
                public void onActivityStarted(Activity activity) {
                    _managerStrategy.onActivityLifecycle("onActivityStarted: ", activity);
                }

                @Override
                public void onActivityResumed(Activity activity) {
                    _managerStrategy.onActivityLifecycle("onActivityResumed: ", activity);
                }

                @Override
                public void onActivityPaused(Activity activity) {
                    _managerStrategy.onActivityLifecycle("onActivityPaused: ", activity);

                }

                @Override
                public void onActivityStopped(Activity activity) {
                    _managerStrategy.onActivityLifecycle("onActivityStopped: ", activity);
                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                    _managerStrategy.onActivityLifecycle("onActivitySaveInstanceState: ", activity);
                }

                @Override
                public void onActivityDestroyed(Activity activity) {
                    _managerStrategy.onActivityLifecycle("onActivityDestroyed: ", activity);
                }
            });

        }
    }

}
