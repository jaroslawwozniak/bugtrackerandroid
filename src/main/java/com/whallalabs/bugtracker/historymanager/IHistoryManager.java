package com.whallalabs.bugtracker.historymanager;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-16.
 */
public interface IHistoryManager {
    void onInit();

    void addSessionCustomData(String key, String value);
}
