package com.whallalabs.bugtracker.historymanager.strategy;

import android.app.Activity;

import com.whallalabs.bugtracker.BugTracker;
import com.whallalabs.bugtracker.history.entry.DefaultHistoryEntry;
import com.whallalabs.bugtracker.history.entry.IHistoryEntry;
import com.whallalabs.bugtracker.history.entry.error.IError;
import com.whallalabs.bugtracker.history.entry.session.ISession;
import com.whallalabs.bugtracker.history.holder.ISessionHistoryHolder;
import com.whallalabs.bugtracker.model.ModelFactory;
import com.whallalabs.bugtracker.sender.ISender;
import com.whallalabs.bugtracker.sender.ISenderCallbacks;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-16.
 */
public class DefaultRestHistoryManagerStrategy extends HistoryManagerStrategy {


    private ISenderCallbacks _senderSendCallbacks = new ISenderCallbacks() {
        @Override
        public void onSuccess(IHistoryEntry historyEntry) {
            historyEntry.setInitiatedOnRemote(true);
            historyEntry.setHasDataToSend(false);
            _sessionHistoryHolder.onEntryUpdated(historyEntry);
        }

        @Override
        public void onFailure(IHistoryEntry historyEntry) {
            _sessionHistoryHolder.onEntryUpdated(historyEntry);
        }
    };

    public DefaultRestHistoryManagerStrategy(ISessionHistoryHolder sessionHistoryHolder, ISender sender) {
        super(sessionHistoryHolder, sender);
    }

    @Override
    public void onAppStart() {
        putInfoSession();
        sendAllSessions();
    }

    @Override
    public void onErrorHandled(IError error) {
        _sessionHistoryHolder.addError(error);
    }

    @Override
    public void onActivityLifecycle(String eventName, Activity activity) {
        _sessionHistoryHolder.addTrackEvent(activity.getClass().getName() + " | " + eventName);
    }

    @Override
    public void onAddSessionCustomData(String key, String value) {
        IHistoryEntry he = _sessionHistoryHolder.getCurrentHistoryEntry();
        if (he != null) {
            ISession currentSession = he.getSession();
            currentSession.addCustomData(key, value);
        }
    }

    private void sendAllSessions() {
        for (IHistoryEntry entry : _sessionHistoryHolder.getEntries()) {
            if (!entry.isInitiatedOnRemote() || entry.hasDataToSend()) {
                _sender.postEntry(entry, _senderSendCallbacks);
            } else {
                _sessionHistoryHolder.deleteEntry(entry);
            }
        }
    }

    private void putInfoSession() {
        ModelFactory modelFactory = new ModelFactory(BugTracker.getApplication());
        ISession session = modelFactory.getSession(ModelFactory.SessionType.DEFAULT);
        IHistoryEntry historyEntry = new DefaultHistoryEntry(session);
        _sessionHistoryHolder.addEntry(historyEntry);
    }

}
