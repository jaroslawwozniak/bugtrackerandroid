package com.whallalabs.bugtracker.historymanager.strategy;

import android.app.Activity;

import com.whallalabs.bugtracker.history.entry.error.IError;
import com.whallalabs.bugtracker.history.holder.ISessionHistoryHolder;
import com.whallalabs.bugtracker.sender.ISender;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-16.
 */
public abstract class HistoryManagerStrategy {

    protected ISessionHistoryHolder _sessionHistoryHolder;
    protected ISender _sender;

    public HistoryManagerStrategy(ISessionHistoryHolder sessionHistoryHolder, ISender sender) {
        _sessionHistoryHolder = sessionHistoryHolder;
        _sender = sender;
    }

    public abstract void onAppStart();

    public abstract void onErrorHandled(IError error);

    public abstract void onActivityLifecycle(String eventName, Activity activity);

    public abstract void onAddSessionCustomData(String key, String value);
}
