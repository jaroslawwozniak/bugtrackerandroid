package com.whallalabs.bugtracker;

import com.whallalabs.bugtracker.errorhandler.IHandlerEventsListener;
import com.whallalabs.bugtracker.history.customdata.ICustomData;
import com.whallalabs.bugtracker.model.CustomData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-23.
 */
public class HandledErrorBuilder {

    IHandlerEventsListener _handlerEventsListener;
    Throwable _throwable;
    List<ICustomData> _dataList = new ArrayList<>();

    public HandledErrorBuilder(IHandlerEventsListener handlerEventsListener) {
        _handlerEventsListener = handlerEventsListener;
    }

    public HandledErrorBuilder setException(Throwable throwable) {
        _throwable = throwable;
        return this;
    }

    public Throwable getThrowable() {
        return _throwable;
    }

    public HandledErrorBuilder addCustomData(String key, String value) {
        ICustomData customData = new CustomData();
        customData.setKey(key);
        customData.setValue(value);
        _dataList.add(customData);
        return this;
    }

    public List<ICustomData> getCustomData() {
        return _dataList;
    }

    public void create() {
        _handlerEventsListener.onHandledError(this);
    }
}
