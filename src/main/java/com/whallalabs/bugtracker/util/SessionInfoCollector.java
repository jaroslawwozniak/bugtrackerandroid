package com.whallalabs.bugtracker.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;

import com.whallalabs.bugtracker.BugTracker;
import com.whallalabs.bugtracker.model.Environment;
import com.whallalabs.bugtracker.model.SessionInfo;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-15.
 */
public class SessionInfoCollector {

    private static void fillEnvironment(Context context, Environment environment) {
        environment.setOsVersion(Build.VERSION.RELEASE);
        environment.setManufacturer(Build.BRAND);
        environment.setModel(Build.MODEL);
        environment.setRegion(context.getResources().getConfiguration().locale.getCountry());

        Point point = CollectorUtils.getDisplaySize(context);
        environment.setScreenHeight(point.y);
        environment.setScreenWidth(point.x);

        environment.setTotalDiskSpace(CollectorUtils.getTotalInternalMemorySize());
        environment.setProcessorCount(Runtime.getRuntime().availableProcessors());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            environment.setProcessorArchitecture(Build.SUPPORTED_ABIS[0]);
        } else {
            environment.setProcessorArchitecture(Build.CPU_ABI);
        }

        environment.setTotalMemory(CollectorUtils.getRAMSize());
    }

    public static void fillSessionInfo(Context context, SessionInfo sessionInfo) {
        String sessionId = UUID.randomUUID().toString();
        sessionInfo.setSessionId(sessionId);

        String userId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        sessionInfo.setUserId(userId);

        String versionName;
        try {
            versionName = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionName = "UNKNOWN";
        }
        sessionInfo.setAppVersion(versionName);
        sessionInfo.setSdkVersion(BugTracker.SDK_VERSION);
        sessionInfo.setSessionDate((new Date()).getTime() / 1000);

        Environment env = new Environment();
        fillEnvironment(context, env);
        sessionInfo.setEnvironment(env);

    }

}
