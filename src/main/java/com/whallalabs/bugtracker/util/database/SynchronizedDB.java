package com.whallalabs.bugtracker.util.database;

import android.content.Context;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.KeyIterator;
import com.snappydb.SnappydbException;

import java.io.Serializable;

/**
 * Created by Jarek on 20.05.15.
 */
public class SynchronizedDB {

    private static final String DB_NAME = "bug_tracker_db";
    final Object _lock = new Object();
    Context _context;

    public void init(Context context) {
        _context = context;
    }

    public <T extends Serializable> T[] getArray(String s, Class<T> aClass) throws SnappydbException {
        T[] result = null;
        SnappydbException ex = null;

        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                result = db.getArray(s, aClass);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex == null) {
            return result;
        } else {
            throw ex;
        }
    }

    public boolean exists(String key) throws SnappydbException {
        boolean result = false;
        SnappydbException ex = null;

        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                result = db.exists(key);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex == null) {
            return result;
        } else {
            throw ex;
        }
    }

    public <T> T getObject(String var1, Class<T> var2) throws SnappydbException {
        T result = null;
        SnappydbException ex = null;

        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                result = db.getObject(var1, var2);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex == null) {
            return result;
        } else {
            throw ex;
        }
    }

    public void put(String s, Object[] objects) throws SnappydbException {
        SnappydbException ex = null;

        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                db.put(s, objects);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex != null) {
            throw ex;
        }
    }

    public void put(String s, Serializable serializable) throws SnappydbException {
        SnappydbException ex = null;

        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                db.put(s, serializable);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex != null) {
            throw ex;
        }
    }

    public boolean getBoolean(String var1) throws SnappydbException {
        SnappydbException ex = null;
        boolean result = false;
        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                result = db.getBoolean(var1);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex == null) {
            return result;
        } else {
            throw ex;
        }
    }

    public void putBoolean(String s, boolean value) throws SnappydbException {
        SnappydbException ex = null;

        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                db.putBoolean(s, value);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex != null) {
            throw ex;
        }
    }

    public int getInt(String var1, int defaultValue) throws SnappydbException {
        SnappydbException ex = null;
        int result = defaultValue;
        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                result = db.getInt(var1);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex == null) {
            return result;
        } else {
            throw ex;
        }
    }

    public void putInt(String s, int value) throws SnappydbException {
        SnappydbException ex = null;

        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                db.putInt(s, value);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex != null) {
            throw ex;
        }
    }

    public void del(String key) throws SnappydbException {
        SnappydbException ex = null;

        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                db.del(key);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex != null) {
            throw ex;
        }
    }

    public <T extends Serializable> T get(String s, Class<T> aClass) throws SnappydbException {
        T result = null;
        SnappydbException ex = null;

        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                result = db.get(s, aClass);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex == null) {
            return result;
        } else {
            throw ex;
        }
    }

    public double getDouble(String var1) throws SnappydbException {
        SnappydbException ex = null;
        double result = 0;
        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                result = db.getDouble(var1);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex == null) {
            return result;
        } else {
            throw ex;
        }
    }

    public KeyIterator getKeyIterator(String var1) throws SnappydbException {
        SnappydbException ex = null;
        KeyIterator result = null;
        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                result = db.findKeysIterator(var1);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex == null) {
            return result;
        } else {
            throw ex;
        }
    }

    public String[] getKeyArray(String var1) throws SnappydbException {
        SnappydbException ex = null;
        String[] result = null;
        synchronized (_lock) {
            DB db = DBFactory.open(_context, DB_NAME);
            try {
                result = db.findKeys(var1);
            } catch (SnappydbException e) {
                ex = e;
            } finally {
                if (db.isOpen()) {
                    db.close();
                }
            }
        }

        if (ex == null) {
            return result;
        } else {
            throw ex;
        }
    }
}
