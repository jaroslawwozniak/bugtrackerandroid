package com.whallalabs.bugtracker.util.database;

/**
 * Created by Kamil on 2015-04-15.
 */
public class DbHelper {

    private static SynchronizedDB synchronizedDB;

    public static SynchronizedDB getSynchronizedDB() {
        if (synchronizedDB == null) {
            synchronizedDB = new SynchronizedDB();
        }
        return synchronizedDB;
    }

    public static void setSynchronizedDB(SynchronizedDB newDb) {
        synchronizedDB = newDb;
    }

}
