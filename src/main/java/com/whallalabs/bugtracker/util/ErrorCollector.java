package com.whallalabs.bugtracker.util;

import android.util.Log;

import java.util.Date;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-22.
 */
public class ErrorCollector {

    public static void fillErrorInfo(com.whallalabs.bugtracker.model.Error error, Throwable throwable) {
        error.setStackTrace(Log.getStackTraceString(throwable));
        error.setExceptionName(throwable.getClass().getName());
        error.setOccurenceTime((new Date()).getTime() / 1000);
    }

}
