package com.whallalabs.bugtracker.model;

import com.google.gson.annotations.Expose;
import com.whallalabs.bugtracker.history.customdata.ICustomData;
import com.whallalabs.bugtracker.history.entry.error.IError;
import com.whallalabs.bugtracker.history.entry.session.ISession;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public class Session implements ISession {

    @Expose
    private SessionInfo sessionInfo;
    @Expose
    private List<IError> errors = new ArrayList<>();

    /**
     * @return The sessionInfo
     */
    public SessionInfo getSessionInfo() {
        return sessionInfo;
    }

    /**
     * @param sessionInfo The sessionInfo
     */
    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    /**
     * @return The errors
     */
    public List<IError> getErrors() {
        return errors;
    }

    /**
     * @param errors The errors
     */
    public void setErrors(List<IError> errors) {
        this.errors = errors;
    }

    @Override
    public List<IError> getErrorList() {
        return errors;
    }

    @Override
    public void addCustomData(String key, String value) {
        if (sessionInfo != null) {
            List<ICustomData> list = sessionInfo.getCustomData();
            ICustomData cd = new CustomData();
            cd.setKey(key);
            cd.setValue(value);
            list.add(cd);
        }
    }
}