package com.whallalabs.bugtracker.model;

import com.google.gson.annotations.Expose;
import com.whallalabs.bugtracker.history.entry.trackevent.ITrackEvent;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */

public class TrackEvent implements ITrackEvent {

    @Expose
    private Long time;
    @Expose
    private String event;

    /**
     * @return The time
     */
    @Override
    public Long getTime() {
        return time;
    }

    /**
     * @param time The time
     */
    @Override
    public void setTime(Long time) {
        this.time = time;
    }

    /**
     * @return The event
     */
    @Override
    public String getEvent() {
        return event;
    }

    /**
     * @param event The event
     */
    @Override
    public void setEvent(String event) {
        this.event = event;
    }

}