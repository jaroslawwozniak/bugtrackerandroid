package com.whallalabs.bugtracker.model;

import android.content.Context;

import com.whallalabs.bugtracker.history.customdata.ICustomData;
import com.whallalabs.bugtracker.history.entry.error.IError;
import com.whallalabs.bugtracker.history.entry.session.ISession;
import com.whallalabs.bugtracker.util.ErrorCollector;
import com.whallalabs.bugtracker.util.SessionInfoCollector;

import java.util.Date;
import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public class ModelFactory {

    public enum ErrorType {
        DEFAULT_HANDLED,
        DEFAULT_UNHANDLED
    }

    public enum SessionType {
        DEFAULT
    }

    Context _context;

    public ModelFactory(Context context) {
        _context = context;
    }

    public IError getError(ErrorType errorType, Throwable throwable) {
        return getError(errorType, throwable, null);
    }

    public IError getError(ErrorType errorType, Throwable throwable, List<ICustomData> customData) {
        IError result = null;
        switch (errorType) {
            case DEFAULT_HANDLED: {
                result = getDefaultHandledError(throwable);
                break;
            }
            case DEFAULT_UNHANDLED: {
                result = getDefaultUnhandledError(throwable);
                break;
            }
        }

        if (result != null && customData != null) {
            result.setCustomData(customData);
        }

        return result;
    }

    public ISession getSession(SessionType sessionType) {
        ISession result = null;
        switch (sessionType) {
            case DEFAULT: {
                result = getDefaultSession();
                break;
            }

        }
        return result;
    }

    private ISession getDefaultSession() {
        Session session = new Session();

        SessionInfo sessionInfo = new SessionInfo();
        SessionInfoCollector.fillSessionInfo(_context, sessionInfo);

        session.setSessionInfo(sessionInfo);

        return session;
    }

    private IError getDefaultHandledError(Throwable throwable) {
        Error result = new Error();
        ErrorCollector.fillErrorInfo(result, throwable);
        result.setHandled(true);
        return result;
    }

    private IError getDefaultUnhandledError(Throwable throwable) {
        Error result = new Error();
        ErrorCollector.fillErrorInfo(result, throwable);
        result.setHandled(false);
        return result;
    }

    public TrackEvent getTrackEvent(String name) {
        TrackEvent result = new TrackEvent();
        result.setEvent(name);
        result.setTime((new Date()).getTime() / 1000);

        return result;
    }

}
