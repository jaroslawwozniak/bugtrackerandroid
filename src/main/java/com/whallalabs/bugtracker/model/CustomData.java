package com.whallalabs.bugtracker.model;

import com.google.gson.annotations.Expose;
import com.whallalabs.bugtracker.history.customdata.ICustomData;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */

public class CustomData implements ICustomData {

    @Expose
    private String key;
    @Expose
    private String value;

    /**
     * @return The key
     */
    @Override
    public String getKey() {
        return key;
    }

    /**
     * @param key The key
     */
    @Override
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return The value
     */
    @Override
    public String getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    @Override
    public void setValue(String value) {
        this.value = value;
    }

}