package com.whallalabs.bugtracker.model;

import com.google.gson.annotations.Expose;
import com.whallalabs.bugtracker.history.customdata.ICustomData;
import com.whallalabs.bugtracker.history.entry.error.IError;
import com.whallalabs.bugtracker.history.entry.trackevent.ITrackEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public class Error implements IError {

    @Expose
    private String stackTrace;
    @Expose
    private String exceptionName;
    @Expose
    private Long occurenceTime;
    @Expose
    private Boolean isHandled;
    @Expose
    private List<ICustomData> customData = new ArrayList<>();
    @Expose
    private List<ITrackEvent> trackEvents = new ArrayList<>();

    /**
     * @return The stackTrace
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * @param stackTrace The stackTrace
     */
    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    /**
     * @return The exceptionName
     */
    public String getExceptionName() {
        return exceptionName;
    }

    /**
     * @param exceptionName The exceptionName
     */
    public void setExceptionName(String exceptionName) {
        this.exceptionName = exceptionName;
    }

    /**
     * @return The occurenceTime
     */
    public Long getOccurenceTime() {
        return occurenceTime;
    }

    /**
     * @param occurenceTime The occurenceTime
     */
    public void setOccurenceTime(Long occurenceTime) {
        this.occurenceTime = occurenceTime;
    }

    /**
     * @return The isHandled
     */
    public Boolean getIsHandled() {
        return isHandled;
    }

    /**
     * @param isHandled The isHandled
     */
    public void setIsHandled(Boolean isHandled) {
        this.isHandled = isHandled;
    }

    /**
     * @return The customData
     */
    public List<ICustomData> getCustomData() {
        return customData;
    }

    /**
     * @param customData The customData
     */
    @Override
    public void setCustomData(List<ICustomData> customData) {
        this.customData = customData;
    }

    @Override
    public boolean isHandled() {
        return getIsHandled();
    }

    @Override
    public void setHandled(boolean isHandled) {
        setIsHandled(isHandled);
    }

    /**
     * @return The trackEvents
     */
    public List<ITrackEvent> getTrackEvents() {
        return trackEvents;
    }

    /**
     * @param trackEvents The trackEvents
     */
    public void setTrackEvents(List<ITrackEvent> trackEvents) {
        this.trackEvents = trackEvents;
    }
}