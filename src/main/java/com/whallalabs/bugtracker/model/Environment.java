package com.whallalabs.bugtracker.model;

import com.google.gson.annotations.Expose;
import com.whallalabs.bugtracker.history.customdata.ICustomData;
import com.whallalabs.bugtracker.history.customdata.ICustomDataAddition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */

public class Environment implements ICustomDataAddition {

    @Expose
    private String osVersion;
    @Expose
    private String manufacturer;
    @Expose
    private String model;
    @Expose
    private String region;
    @Expose
    private Integer screenHeight;
    @Expose
    private Integer screenWidth;
    @Expose
    private Long totalMemory;
    @Expose
    private Integer processorCount;
    @Expose
    private String processorArchitecture;
    @Expose
    private Long totalDiskSpace;
    @Expose
    private List<ICustomData> customData = new ArrayList<>();

    /**
     * @return The osVersion
     */
    public String getOsVersion() {
        return osVersion;
    }

    /**
     * @param osVersion The osVersion
     */
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    /**
     * @return The manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * @param manufacturer The manufacturer
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * @return The model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model The model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return The region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region The region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return The screenHeight
     */
    public Integer getScreenHeight() {
        return screenHeight;
    }

    /**
     * @param screenHeight The screenHeight
     */
    public void setScreenHeight(Integer screenHeight) {
        this.screenHeight = screenHeight;
    }

    /**
     * @return The screenWidth
     */
    public Integer getScreenWidth() {
        return screenWidth;
    }

    /**
     * @param screenWidth The screenWidth
     */
    public void setScreenWidth(Integer screenWidth) {
        this.screenWidth = screenWidth;
    }

    /**
     * @return The totalMemory
     */
    public Long getTotalMemory() {
        return totalMemory;
    }

    /**
     * @param totalMemory The totalMemory
     */
    public void setTotalMemory(Long totalMemory) {
        this.totalMemory = totalMemory;
    }

    /**
     * @return The processorCount
     */
    public Integer getProcessorCount() {
        return processorCount;
    }

    /**
     * @param processorCount The processorCount
     */
    public void setProcessorCount(Integer processorCount) {
        this.processorCount = processorCount;
    }

    /**
     * @return The processorArchitecture
     */
    public String getProcessorArchitecture() {
        return processorArchitecture;
    }

    /**
     * @param processorArchitecture The processorArchitecture
     */
    public void setProcessorArchitecture(String processorArchitecture) {
        this.processorArchitecture = processorArchitecture;
    }

    /**
     * @return The totalDiskSpace
     */
    public Long getTotalDiskSpace() {
        return totalDiskSpace;
    }

    /**
     * @param totalDiskSpace The totalDiskSpace
     */
    public void setTotalDiskSpace(Long totalDiskSpace) {
        this.totalDiskSpace = totalDiskSpace;
    }

    /**
     * @return The customData
     */
    @Override
    public List<ICustomData> getCustomData() {
        return customData;
    }

    /**
     * @param customData The customData
     */
    @Override
    public void setCustomData(List<ICustomData> customData) {
        this.customData = customData;
    }

}