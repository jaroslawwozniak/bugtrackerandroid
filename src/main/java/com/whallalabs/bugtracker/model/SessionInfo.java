package com.whallalabs.bugtracker.model;

import com.google.gson.annotations.Expose;
import com.whallalabs.bugtracker.history.customdata.ICustomData;
import com.whallalabs.bugtracker.history.customdata.ICustomDataAddition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public class SessionInfo implements ICustomDataAddition {

    @Expose
    private String sessionId;
    @Expose
    private String userId;
    @Expose
    private String appVersion;
    @Expose
    private String sdkVersion;
    @Expose
    private Long sessionDate;
    @Expose
    private Environment environment;
    @Expose
    private List<ICustomData> customData = new ArrayList<>();

    /**
     * @return The sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId The sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The appVersion
     */
    public String getAppVersion() {
        return appVersion;
    }

    /**
     * @param appVersion The appVersion
     */
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    /**
     * @return The sdkVersion
     */
    public String getSdkVersion() {
        return sdkVersion;
    }

    /**
     * @param sdkVersion The sdkVersion
     */
    public void setSdkVersion(String sdkVersion) {
        this.sdkVersion = sdkVersion;
    }

    /**
     * @return The sessionDate
     */
    public Long getSessionDate() {
        return sessionDate;
    }

    /**
     * @param sessionDate The sessionDate
     */
    public void setSessionDate(Long sessionDate) {
        this.sessionDate = sessionDate;
    }

    /**
     * @return The environment
     */
    public Environment getEnvironment() {
        return environment;
    }

    /**
     * @param environment The environment
     */
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    /**
     * @return The customData
     */
    public List<ICustomData> getCustomData() {
        return customData;
    }

    /**
     * @param customData The customData
     */
    public void setCustomData(List<ICustomData> customData) {
        this.customData = customData;
    }

}