package com.whallalabs.bugtracker;

import android.app.Application;
import android.util.Log;

import com.whallalabs.bugtracker.errorhandler.UncaughtGlobalExceptionHandler;
import com.whallalabs.bugtracker.history.holder.ISessionHistoryHolder;
import com.whallalabs.bugtracker.history.holder.SessionHistorySnappyDBHolder;
import com.whallalabs.bugtracker.historymanager.ErrorHistoryManager;
import com.whallalabs.bugtracker.historymanager.strategy.DefaultRestHistoryManagerStrategy;
import com.whallalabs.bugtracker.historymanager.strategy.HistoryManagerStrategy;
import com.whallalabs.bugtracker.model.ModelFactory;
import com.whallalabs.bugtracker.sender.ISender;
import com.whallalabs.bugtracker.sender.apisender.BugTrackerAPISender;
import com.whallalabs.bugtracker.sender.restadapter.BugTrackerAdapterHolderStrategy;
import com.whallalabs.bugtracker.sender.restadapter.IRestAdapterStrategy;
import com.whallalabs.bugtracker.sender.restadapter.RestAdapterHolder;
import com.whallalabs.bugtracker.util.database.DbHelper;

public class BugTracker {

    public static final String SDK_VERSION = "0.9.1";
    public static final String LOGCAT_TAG = "BugTrackerLog";
    private static final String CACHED_ENTRIES_COUNT_KEY = "Cached entries count:";
    private static ErrorHistoryManager _errorHistoryManager;
    private static Application _application;
    private static BugTrackerConfig _bugTrackerConfig;

    public static void init(Application application, BugTrackerConfig bugTrackerConfig) {
        if (_errorHistoryManager == null) {
            _application = application;
            _bugTrackerConfig = bugTrackerConfig;

            IRestAdapterStrategy restAdapterStrategy = new BugTrackerAdapterHolderStrategy(bugTrackerConfig.getApiUrl(), _bugTrackerConfig.getHeaders());

            RestAdapterHolder.getStaticInstance().init(restAdapterStrategy);
            DbHelper.getSynchronizedDB().init(application);

            ModelFactory modelFactory = new ModelFactory(application);
            ISessionHistoryHolder sessionHistoryHolder = new SessionHistorySnappyDBHolder(modelFactory);
            ISender sender = new BugTrackerAPISender();

            HistoryManagerStrategy historyManagerStrategy = new DefaultRestHistoryManagerStrategy(sessionHistoryHolder, sender);

            _errorHistoryManager = new ErrorHistoryManager(historyManagerStrategy,
                    modelFactory,
                    _application
            );
            UncaughtGlobalExceptionHandler errorHandler = new UncaughtGlobalExceptionHandler(_errorHistoryManager);

            addSessionCustomData(CACHED_ENTRIES_COUNT_KEY, String.valueOf(sessionHistoryHolder.getEntriesLoadedFromStorageCount()));
            _errorHistoryManager.onInit();
            Log.d(LOGCAT_TAG, "init");
        }
    }

    public static HandledErrorBuilder handleException(Throwable ex) {
        HandledErrorBuilder builder = new HandledErrorBuilder(_errorHistoryManager);
        return builder.setException(ex);
    }

    public static Application getApplication() {
        return _application;
    }

    public static void addSessionCustomData(String key, String value) {
        _errorHistoryManager.addSessionCustomData(key, value);
    }

}
