package com.whallalabs.bugtracker.errorhandler;

import com.whallalabs.bugtracker.HandledErrorBuilder;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-14.
 */
public interface IHandlerEventsListener {
    void onUnhandledError(Thread thread, Throwable throwable);
    void onHandledError(HandledErrorBuilder handledErrorBuilder);
}
