package com.whallalabs.bugtracker.errorhandler;

/**
 * Created by Jaroslaw Wozniak  on 2015-07-13.
 */
public class UncaughtGlobalExceptionHandler implements Thread.UncaughtExceptionHandler {

    Thread.UncaughtExceptionHandler _defaultExceptionHandler;
    IHandlerEventsListener _handlerEvents;

    public UncaughtGlobalExceptionHandler(IHandlerEventsListener handlerEvents) {
        _handlerEvents = handlerEvents;
        _defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        try {
            _handlerEvents.onUnhandledError(thread, ex);
            _defaultExceptionHandler.uncaughtException(thread, ex);
        } catch (Throwable e) {
            if (_defaultExceptionHandler != null) {
                _defaultExceptionHandler.uncaughtException(thread, ex);
            }
        }
    }
}
